.. Battelle documentation master file, created by
   sphinx-quickstart on Tue Mar 30 10:12:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Battelle's documentation!
====================================

`Battelle` is a python library designed to create, train and test
simple artificial intelligence models.

It was created as a project for the 2021-2022 Artificial Intelligence course
of the University of Geneva.

`Battelle` supports neural networks,
perceptrons and KNN classifiers.

Installation
------------

`Battelle` can be installed with `pip`:

.. code::

   $ cd battelle
   $ pip install .

It is recommended to work in a virtual environment.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Documentation
-------------

.. toctree::
   :maxdepth: 2

   nn
   knn
   perceptron

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
