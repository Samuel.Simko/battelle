.. battelle.perceptron

.. currentmodule:: battelle.perceptron

Perceptron subpackage (battelle.perceptron)
===========================================

.. autosummary::
   :toctree: generated/

   Perceptron

   .. automethod::
      
      Perceptron.train
      Perceptron.predict
