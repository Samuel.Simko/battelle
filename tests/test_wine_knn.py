# -*- coding: utf-8 -*-
from turtle import distance
from tqdm import tqdm
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from battelle.knn import KNearestNeighborsClassifier

# open mushroom database

data = pd.read_csv("../data/winequality-white.csv", delimiter = ";").values
[train, test] = train_test_split(data, train_size=0.8)
X = train[:,:-1]
y = train[:,-1]
test_X = test[:,:-1]
test_y = test[:,-1]

knn = KNearestNeighborsClassifier(n_neighbors=7, weights="distance")
knn.train(X,y)

counter = 0
n = len(test_X)  
for i in tqdm(range(n)) :
    if knn.predict(test_X[i]) == test_y[i] :
        counter += 1
      
print('{}/{} - {}%'.format(counter,n,100*counter/n))
