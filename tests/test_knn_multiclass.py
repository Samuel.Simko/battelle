from inspect import ArgSpec
from tkinter import N
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.cm as cm
from sklearn.datasets import make_classification
from tqdm import tqdm

from battelle.knn import KNearestNeighborsClassifier

# Generate data
INPUT_DIM = 2
CLASSES = 4 
x,features = make_classification(
    n_samples=500,
    n_features=INPUT_DIM,
    n_informative=INPUT_DIM,
    n_redundant=0,
    n_repeated=0,
    n_classes=CLASSES,
    n_clusters_per_class=1,
    class_sep=2.0
) 

y = []
for i,f in enumerate(features) :
    y.append([f,1 * (x[i][0] * x[i][1] >= 0) ])
    
# Initialize and train the neural network
knn = KNearestNeighborsClassifier()
knn.train(x,y)

colors = ['#1ECBE1','#961EE1','#E1341E','#6AE11E']
cmap = LinearSegmentedColormap.from_list('tetradic', colors=colors, N=CLASSES)

RES = 100
min_x = min(x[:,0])
max_x = max(x[:,0])
min_y = min(x[:,1])
max_y = max(x[:,1])

xrange = np.linspace(min_x, max_x, RES)
yrange = np.linspace(min_y, max_y, RES)
z1 = []
z2 = []
for y_coord in tqdm(yrange) :
    row1 = []
    row2 = []
    for x_coord in xrange :
        p = knn.predict([x_coord, y_coord])
        row1.append(p[0]/4.)
        row2.append(p[1])
    z1.append(row1)
    z2.append(row2)
    
plt.subplots(2,2,figsize=(6,6))
plt.subplot(2,2,1)
plt.title('Clusters')
plt.imshow(np.rot90(np.transpose(z1)), cmap=cmap, extent=(min_x,max_x,min_y,max_y), alpha=0.3)
for i, xi in enumerate(x):
    plt.plot(xi[0], xi[1], '.', color=colors[y[i][0]])    

plt.subplot(2,2,2)
plt.title('XOR')
plt.imshow(np.rot90(np.transpose(z2)), cmap=cm.coolwarm, extent=(min_x,max_x,min_y,max_y), alpha=0.3)
for i, xi in enumerate(x):
    if y[i][1] == 0:
        plt.plot(xi[0], xi[1], '.b')
    elif y[i][1] == 1:
        plt.plot(xi[0], xi[1], '.r')
        
plt.subplot(2,2,3)
gradient = np.linspace(0, 1, 256)
gradient = np.vstack((gradient, gradient))
plt.imshow(gradient, extent=(0,4,0,0.4) , cmap=cmap)
plt.xticks(range(5))
ax = plt.gca()
ax.axes.yaxis.set_visible(False)

plt.subplot(2,2,4)
plt.imshow(gradient, extent=(0,1,0,0.1) , cmap=cm.coolwarm)
plt.xticks(range(2))
ax = plt.gca()
ax.axes.yaxis.set_visible(False)

plt.tight_layout()
plt.show()
