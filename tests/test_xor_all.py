from re import sub
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.axes as ax

from battelle.nn import NeuralNetwork , Layer
from battelle.knn import KNearestNeighborsClassifier
from battelle.perceptron import Perceptron

# List of features
input_dim = 2
x = np.random.random(size=(300,input_dim)) - 0.5
x = [list(xi) for xi in x]

# List of corresponding labels
# y = np.array([1 * (np.prod(k) >= 0) for k in x]) # xor
# y = np.array([1*(k[0] >= 0 and k[1] >= 0) for k in x]) # and
y = np.array([1 * (np.sum(k) >= 0) for k in x]) # diagonal

# Initialize and train the neural network
nn = NeuralNetwork()
nn.add_layer(Layer(5, 'tanh', input_dim))
nn.add_layer(Layer(4, 'tanh'))
nn.add_layer(Layer(1, 'tanh'))

NUM_EPOCHS = 100
for i in range(NUM_EPOCHS):
    print(f"Epoch {i+1}/{NUM_EPOCHS}")
    nn.train(x, y, learning_rate=0.03)
    print(f"Loss: {np.mean([(nn.predict(x[k]) - y[k])**2 for k in range(len(x))])}")

# Initialize and train the k-nearest neighbors classifier
knn = KNearestNeighborsClassifier()
knn.train(x,y)

# Initialize and train the perceptron
weights = np.array(0.5 - np.random.random(size=(input_dim+1)))
p = Perceptron(weights)
p.train(x,y)

# Declare subplots
f = plt.figure(figsize=(10,6))
ax_nn = f.add_subplot(1,3,1)
ax_knn = f.add_subplot(1,3,2)
ax_p = f.add_subplot(1,3,3)

classifiers = [
    (ax_nn, nn, "Neural network"),
    (ax_knn, knn, "k-NN"),
    (ax_p,p, "Perceptron")
]

for subplot, classifier, name in classifiers :
    
    subplot.set_title(name + " result")
    subplot.set_aspect(1)
    subplot.set_xlabel(f"Loss: {np.mean([(classifier.predict(x[k]) - y[k])**2 for k in range(len(x))])}")
    
    # Plot training data
    for i, xi in enumerate(x):
        if y[i] == 1:
            subplot.plot(xi[0], xi[1], 'xr')
        else:
            subplot.plot(xi[0], xi[1], 'xb')

    # Plot contour 
    RES = 100
    xcontour = np.linspace(-0.5, 0.5, RES)
    ycontour = np.linspace(-0.5, 0.5, RES)
    z = []
    for x_coord in xcontour :
        row = []
        for y_coord in ycontour :
            row.append(classifier.predict([x_coord, y_coord]))
        z.append(row)
    z = np.transpose(z)        
    subplot.contourf(xcontour,ycontour,z, levels = 2, cmap=cm.coolwarm, alpha=0.5)

plt.tight_layout(h_pad=0.5)
plt.show()
