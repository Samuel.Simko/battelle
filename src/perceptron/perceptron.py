import numpy as np

class Perceptron(object):
    """A Perceptron neuron

    Attributes
    ----------
    weights : array
        The initial weights of the neuron

    """

    def __init__(self, weights):
        self.__weights = weights

    @property
    def weights(self):
        return self.__weights

    @weights.setter
    def weights(self, weights):
        self.__weights = weights

    def train(self, x, y, learning_rate=0.01, nb_iter=1000):
        """Train Perceptron with dataset of features `x` and of corresponding labels `y`
        using the Perceptron algorithm.

        Parameters
        ----------
        x: np.array
            A list of features
        y: np.array
            The corresponding list of labels.
        learning_rate: float
            The learning rate.
        nb_iter: int
            The number of learning iterations.
        """
        for _ in range(nb_iter):
            augmented_x = np.c_[x,np.ones(len(x))]
            for i, xi in enumerate(augmented_x):
                tmp_y = 1 if np.dot(xi, self.weights) >= 0 else 0
                self.weights += (learning_rate * (y[i] - tmp_y)) * np.array(xi)

    def predict(self, x):
        """Makes a prediction for features `x`

        Parameters
        ----------
        x: np.array
            The features for which a prediction should be made

        Returns
        -------
        int
            The predicted label.
        """

        return 1 if np.dot(x, self.weights[:-1]) + self.weights[-1] >= 0 else 0

