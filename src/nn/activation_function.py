# -*- coding: utf-8 -*-

import numpy as np


class ActivationFunction:
    """An instance of a activation function

    An activation function is a function which is applied to the output signal
    of a neuron.

    Attributes
    ----------
    f: (float) -> float
        The activation function

    fp: (float) -> float
        The first degree derivative of the activation function

    """

    def __init__(self, f, fp):
        self.f = f
        self.fp = fp


def sigmoid_f(x):
    """Sigmoid function"""
    return 1 / (1 + np.exp(-x))


def sigmoid_fp(x):
    """Derivative of sigmoid function"""
    return sigmoid_f(x) * (1 - sigmoid_f(x))


def tanh_fp(x):
    """Derivative of tanh function"""
    return 1 - np.tanh(x) ** 2


sigmoid = ActivationFunction(sigmoid_f, sigmoid_fp)
tanh = ActivationFunction(np.tanh, tanh_fp)
