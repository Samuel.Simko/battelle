# -*- coding: utf-8 -*-

import numpy as np

from .neuron import Neuron

from .activation_function import ActivationFunction
from .activation_function import sigmoid
from .activation_function import tanh


class Layer(object):
    """An instance of a layer

    A layer contains a number of neurons.
    Each layer has an input dimension, which must be specified if
    it is the first layer of a neural network, and an activation function.

    Attributes
    ----------
    neuron_count: int
        The number of neurons in the layer
    activation_function : String
        Activation function to use (tanh, sigmoid...)
    input_dim: int
        The number of input dimensions
    """

    def __init__(self, neuron_count, activation_function="sigmoid", input_dim=None):
        self.activation_function = activation_function
        self.__neurons = [
            Neuron(
                0.5 - np.random.random(input_dim),
                0.5 - np.random.random(),
                self.activation_function,
            )
            for _ in range(neuron_count)
        ]
        self.__input_dim = input_dim
        self.__neuron_count = neuron_count

    @property
    def input_dim(self):
        return self.__input_dim

    @property
    def neuron_count(self):
        return self.__neuron_count

    @property
    def neurons(self):
        return self.__neurons

    @property
    def activation_function(self):
        return self.__activation_function

    @activation_function.setter
    def activation_function(self, value):
        if not isinstance(value, ActivationFunction):
            if value == "sigmoid":
                self.__activation_function = sigmoid
            elif value == "tanh":
                self.__activation_function = tanh
            else:
                raise ValueError("Invalid activation function")
            return
        self.__activation_function = value

    def forward(self, x):
        """Returns the output of each neuron of the layer for input `x`."""
        return [neuron.forward(x) for neuron in self.__neurons]
