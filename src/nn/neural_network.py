# -*- coding: utf-8 -*-

import numpy as np

from .layer import Layer
from tqdm import tqdm

from pickle import dump
from pickle import load
from pickle import HIGHEST_PROTOCOL


class NeuralNetwork(object):
    """Instance of a neural network.

    Attributes
    ----------
    layers : List<Layer>
        A list of every layer in the neural network
    """

    def __init__(self):
        self.__layers = []
        self.all_weights = []

    @property
    def layers(self):
        return self.__layers

    def add_layer(self, layer):
        """Adds a layer to the neural network

        Note
        ----
        The first layer to be added to the network must have a
        valid `input_dim`.


        Parameters
        ----------
        layer
            The layer to be added

        Returns
        -------
        bool
            True if successful, False otherwise.
        """
        # Raise error if layer is first and has no input dim
        if len(self.__layers) == 0 and layer.input_dim is None:
            raise ValueError("First layer must have input_dim specified")

        # Put right layer input dimension if not specified
        if layer.input_dim is None:
            self.__layers.append(
                Layer(
                    layer.neuron_count,
                    layer.activation_function,
                    self.__layers[-1].neuron_count,
                )
            )
        else:
            self.__layers.append(layer)

        return True

    @property
    def __n_outputs(self):
        """Returns the number of output neurons

        Returns
        -------
        int
            The number of output neurons
        """
        return self.__layers[-1].neuron_count

    def predict(self, x):
        """Makes a prediction for features `x`

        Parameters
        ----------
        x: np.array
            The features for which a prediction should be made

        Returns
        -------
        np.array
            The predicted label array
        """
        for layer in self.__layers:
            x = layer.forward(x)
        return x[0] if self.__n_outputs == 1 and len(x) == 1 else np.array(x)

    def print_weights(self):
        """Prints the weights of all neurons of the neural net
        (used for debugging purposes)
        """
        all_weights = []
        for l, layer in enumerate(self.__layers):
            weights_one_layer = []
            for n, neuron in enumerate(layer.neurons):
                # print((l, n), neuron.weights)
                weights_one_layer.append(neuron.weights)
            all_weights.append(weights_one_layer)
        self.all_weights = all_weights

    def predict_steps(self, x):
        """Makes a prediction for features `x`, while remembering the
        intermediate values of `x` in each layer

        Parameters
        ----------
        x: np.array
            The features for which a prediction should be made

        Returns
        -------
        np.array
            An array containing the input `x` of each layer
        """
        allx = [x]
        for layer in self.__layers:
            x = layer.forward(x)
            allx.append(x)
        return allx

    def save_to_file(self, filename):
        """Save current network to file `filename`.

        Parameters
        ----------
        filename: string
            The filename of the saved network

        Returns
        -------
        bool
            True if the save success, otherwise False
        """

        with open(filename, "wb") as f:
            dump(self, f, HIGHEST_PROTOCOL)

        return True

    @staticmethod
    def load_from_file(filename):
        """Load a network from file `filename`.

        Parameters
        ----------
        filename: string
            The filename of the saved network

        Returns
        -------
        bool
            True if the load was successful, otherwise False
        """

        with open(filename, "rb") as f:
            self = load(f)

        return self

    def backward(self, x, label, learning_rate=0.05):
        """Updates weights for features `x` and label `label`
        using the backpropagation algorithm.

        Parameters
        ----------
        x: np.array
            The features for which a prediction should be made
        label:
            The corresponding label
        """
        # Get intermediate values of `x`
        allx = self.predict_steps(x)
        prediction = allx[-1]

        # calculate error for last layer
        last_layer = self.__layers[-1]
        h = [neuron.h for neuron in last_layer.neurons]
        e = [
            last_layer.activation_function.fp(h[i])
            * (np.array(prediction[i]) - np.array(label[i]))
            for i in range(len(prediction))
        ]
        w = [neuron.weights for neuron in last_layer.neurons]
        alle = [e]

        # Backpropagate error on previous layers
        if len(self.__layers) > 1:
            for index, layer in enumerate(reversed(self.__layers[:-1])):

                h = [neuron.h for neuron in layer.neurons]
                w = [neuron.weights for neuron in last_layer.neurons]
                e = [
                    layer.activation_function.fp(h[j])
                    * np.sum([w[k][j] * e[k] for k in range(len(e))])
                    for j in range(len(h))
                ]

                alle.append(e)
                last_layer = layer

        # Update neuron weights of all layers
        for i, ri in enumerate(reversed(range(len(self.__layers)))):
            layer = self.__layers[ri]
            for k in range(len(layer.neurons)):
                neuron = layer.neurons[k]

                # Update weights of neuron `k` of layer `ri`
                neuron.weights = [
                    neuron.weights[j] - learning_rate * allx[ri][j] * alle[i][k]
                    for j in range(len(neuron.weights))
                ]
                neuron.bias -= learning_rate * alle[i][k]

    def train(self, x, y, learning_rate=0.05):
        """Train the neural network with dataset of features `x` and of corresponding labels `y`.
        using the backpropagation algorithm.

        Parameters
        ----------
        x: np.array
            A list of features
        y: np.array
            The corresponding list of labels.
        """
        if self.__n_outputs == 1 and np.array(y).ndim == 1:
            old_y = y
            y = []
            for f in old_y:
                y.append([f])
        for i, xi in tqdm(enumerate(x)):
            self.backward(xi, y[i], learning_rate)

    def get_metrics(self, features, labels, binary_function):
        """Compute metrics for a binary classification using data from `features` and `labels`.
        Returns a dictionary containing the keys "precision", "recall",
        "f1_score" and "accuracy".

        Parameters
        ----------
        x: np.array
            A list of features
        y: np.array
            The corresponding list of labels.
        binary_function: (np.array) -> {0,1}
            A function which classifies the output into two categories.
            Category 1 is positive, while the category 0 is negative.

        Returns
        -------
        dict
            A dictionary containing the calculated metrics.
        """
        FP = 0
        TP = 0
        FN = 0
        TN = 0

        for i, f in enumerate(features):
            pred = binary_function(self.predict(f))

            if pred == labels[i] and labels[i] == [0]:
                TN += 1
            elif pred == labels[i] and labels[i] == [1]:
                TP += 1
            elif pred != labels[i] and labels[i] == [1]:
                FN += 1
            elif pred != labels[i] and labels[i] == [0]:
                FP += 1

        print(TN, TP, FN, FP)

        try:
            P = (TP) / (TP + FP)
        except ZeroDivisionError:
            P = 0

        try:
            R = (TP) / (TP + FN)
        except ZeroDivisionError:
            R = 0

        try:
            F1 = (2 * P * R) / (P + R)
        except ZeroDivisionError:
            F1 = 0

        return {
            "accuracy": (TP + TN) / (TP + TN + FP + FN),
            "precision": P,
            "recall": R,
            "f1 score": F1,
        }

    def get_weighted_accuracy(self, features, labels):
        """Compute weighted accuracy for a multiclass classification using data from `features` and `labels`.
        Returns a dictionary containing the key "accuracy".

        Parameters
        ----------
        features: np.array
            A list of features
        labels: np.array
            The corresponding list of labels

        Returns
        -------
        dict
            A dictionary containing the calculated metrics.
        """

        conf_matrix = np.zeros(shape=(len(labels[0]), len(labels[0])))

        for i, f in enumerate(features):
            pred = (self.predict(f)).argmax()
            real = labels[i].argmax()
            conf_matrix[pred][real] += 1

        weights = np.array(
            [sum(conf_matrix[i, :]) for i in range(len(labels[0]))]
        ) / np.sum(conf_matrix)

        R = np.array(
            [
                conf_matrix[i, i] / (np.sum(conf_matrix[i, :]))
                for i in range(len(labels[0]))
            ]
        )
        P = np.array(
            [
                conf_matrix[i, i] / (np.sum(conf_matrix[:, i]))
                for i in range(len(labels[0]))
            ]
        )

        return {
            "accuracy": np.sum(np.diag(conf_matrix)) / np.sum(conf_matrix),
        }
