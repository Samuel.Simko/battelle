# -*- coding: utf-8 -*-

import numpy as np


class Neuron(object):
    """An instance of a neuron

    Attributes
    ----------
    weights : array
        The initial weights of the neuron
    bias : float
        The initial bias value of the neuron
    activation_function : String
        activation function to use (tanh, sigmoid...)

    """

    def __init__(self, weights, bias, activation_function):
        self.__weights = weights
        self.__bias = bias
        self.activation_function = activation_function
        self.__h = None

    @property
    def activation_function(self):
        return self.__activation_function

    @property
    def h(self):
        return self.__h

    @property
    def weights(self):
        return self.__weights

    @weights.setter
    def weights(self, weights):
        self.__weights = weights

    @property
    def bias(self):
        return self.__bias

    @bias.setter
    def bias(self, bias):
        self.__bias = bias

    @activation_function.setter
    def activation_function(self, activation_function):
        self.__activation_function = activation_function

    def forward(self, x):
        """Propagate value `x` through the neuron."""
        self.__h = np.sum(self.__weights * np.array(x)) + self.bias
        return self.activation_function.f(self.__h)
