==================
Battelle
==================

About
-----

Battelle is a python library for artificial intelligence and machine learning.


Installation
------------

.. code-block:: console

   $ pip install -r requirements.txt
   $ pip install --user .


Documentation
-------------

The documentation is available at https://battelle.readthedocs.io.

